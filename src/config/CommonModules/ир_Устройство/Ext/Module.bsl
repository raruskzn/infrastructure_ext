﻿// Содержит методы по работе с устройствами
//  
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Возвращает коллекцию устройств
//	
// Возвращаемое значение: 
//	ФиксированноеСоответствие
//
Функция Коллекция() Экспорт
	
	Возврат ир_НастройкаПовтИсп.УстройстваКоллекцияПолучить();

КонецФункции // Коллекция 

// Возвращает описание нового устройства
//
// Параметры: 
// 	Идентификатор - Строка - Идентификатор устройства (имя компьютера)
// 	Область - Строка - Область сети устройства
// 	Комментарий - Строка - Произвольный комментарий
//	Адреса - ФиксированныйМассив - Список адресов устройства
//
// Возвращаемое значение: 
// 	Структура
//
Функция Создать(Идентификатор, Область = Null, Комментарий = "", Адреса = Null) Экспорт
	
	Результат	= Новый Структура();
	
	Идентификатор(Результат, ИдентификаторВКлюч(Идентификатор));
	Область(Результат, ?(Область = Null, ир_ОбластьПредопределенная.Умолчание(), Область));
	Комментарий(Результат, Комментарий);
	Адреса(Результат, ?(Адреса = Null, Новый ФиксированныйМассив(Новый Массив()), Адреса));
	
	Возврат Результат;
	
КонецФункции // Создать 

// Возвращает описание устройства структурой
//
// Параметры: 
// 	Идентификатор - Строка - Идентификатор устройства (имя компьютера)
//
// Возвращаемое значение: 
// 	Структура, 
//
Функция Получить(Идентификатор) Экспорт
	
	Результат	= Коллекция().Получить(ИдентификаторВКлюч(Идентификатор));
	
	Возврат ?(Результат = Неопределено
	, Результат
	, Новый Структура(Результат));
	
КонецФункции // Получить 

// Записывает устройство
//
// Параметры: 
//	Устройство - Структура - Данные устройства
//
Процедура Записать(Устройство) Экспорт
	
	ир_НастройкаВызовСервера.УстройствоЗаписать(Устройство);
	
КонецПроцедуры // Записать

// Удаляет устройство
//
// Параметры:
//	Идентификатор - Строка - Идентификатор устройства (имя компьютера)
//
Процедура Удалить(Идентификатор) Экспорт
	
	ир_НастройкаВызовСервера.УстройствоУдалить(ИдентификаторВКлюч(Идентификатор));
	
КонецПроцедуры // Удалить

// Возвращает текущее устройство
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Структура, Неопределено
//
Функция Текущее() Экспорт
	
	Возврат Получить(ИмяКомпьютера());
	
КонецФункции // Текущее 

// Истина, если устройство зарегистрировано
//
// Параметры: 
// 	Идентификатор - Строка - Идентификатор устройства
//
// Возвращаемое значение: 
// 	Булево
//
Функция Существует(Идентификатор) Экспорт
	
	Возврат Получить(Идентификатор) <> Неопределено;
	
КонецФункции // Существует 

// Возвращает ключ устройства по идентификатору
//
// Параметры: 
// 	Идентификатор - Строка - Идентификатор устройства
//
// Возвращаемое значение: 
// 	Строка
//
Функция ИдентификаторВКлюч(Идентификатор) Экспорт
	
	Возврат НРег(Идентификатор);
	
КонецФункции // ИдентификаторВКлюч 

// Истина, если у устройства есть доступ к целевой области
//
// Параметры: 
//	Цель - Строка - Целевой адрес
//	Идентификатор - Строка - Идентификатор устройства источника, по умолчанию текущее
//
// Возвращаемое значение: 
//	Булево
//
Функция ДоступЕсть(Цель, Идентификатор = Null) Экспорт
	
	Если НЕ ир_Подсистема.Включена() Тогда
		Возврат Истина;
	КонецЕсли;
	
	Возврат ир_НастройкаПовтИсп.УстройствоДоступЕсть(Цель
	, ?(Идентификатор = Null, ир_Устройство.Идентификатор(ир_Устройство.Текущее()), Идентификатор)); 
	
КонецФункции // ДоступЕсть 

//
// Свойства устройства
// 

// Устанавливает Идентификатор устройства. Возвращает значение на момент до вызова метода
//
// Параметры: 
// 	Устройство - Структура - Исследуемое устройство
// 	Значение - Строка - Новое значение
//
// Возвращаемое значение: 
// 	Строка
//
Функция Идентификатор(Устройство, Значение = Null) Экспорт
	
	Возврат СтруктураСвойство(Устройство, "Идентификатор", Значение);
	
КонецФункции // Идентификатор 

// Устанавливает Комментарий устройства. Возвращает значение на момент до вызова метода
//
// Параметры: 
// 	Устройство - Структура - Исследуемое устройство
// 	Значение - Строка - Новое значение
//
// Возвращаемое значение: 
// 	Строка
//
Функция Комментарий(Устройство, Значение = Null) Экспорт
	
	Возврат СтруктураСвойство(Устройство, "Комментарий", Значение);
	
КонецФункции // Комментарий 

// Устанавливает область устройства. Возвращает значение на момент до вызова метода
//
// Параметры:
//	Устройство - Структура - Исследуемое устройство
// 	Значение - Строка - Новое значение
//
// Возвращаемое значение: 
//	Строка
//
Функция Область(Устройство, Значение = Null) Экспорт
	
	Возврат СтруктураСвойство(Устройство, "Область", Значение);
		
КонецФункции // ОбластьПолучить 

// Устанавливает адреса устройства. Возвращает значение на момент до вызова метода
//
// Параметры:
//	Устройство - Структура - Коллекция свойств
//	Значение - ФиксированныйМассив - Адрес устройства
//
// Возвращаемое значение: 
//	ФиксированныйМассив
Функция Адреса(Устройство, Значение = Null) Экспорт
	
	Возврат СтруктураСвойство(Устройство, "Адреса", Значение);
	
КонецФункции // Адреса 

//
// Адреса устройства
//

// Добавляет адрес устройства
//
// Параметры: 
//	Устройство - Структура - Исследуемое устройство
//	Адрес - Строка - Адрес устройства
//
Процедура АдресДобавить(Устройство, Адрес) Экспорт
	
	ир_НастройкаВызовСервера.УстройствоАдресДобавить(Устройство, Адрес);
	
КонецПроцедуры // АдресДобавить

// Удаляет адрес устройства
//
// Параметры: 
//	Устройство - Структура - Исследуемое устройство
//	Адрес - Строка - Адрес устройства
//
Процедура АдресУдалить(Устройство, Адрес) Экспорт
	
	ир_НастройкаВызовСервера.УстройствоАдресУдалить(Устройство, Адрес);
	
КонецПроцедуры // АдресУдалить
  
#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс


#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Устанавливает значение свойства структуры. Возвращает значение на момент до вызова метода
//
// Параметры: 
// 	Коллекция - Структура - Модифицируемая коллекция
// 	Свойство - Строка - Имя свойства
// 	Значение - Произвольный - Новое значение
//
// Возвращаемое значение: 
// 	Произвольный
//
Функция СтруктураСвойство(Коллекция, Свойство, Значение = Null)
	
	Возврат ир_Модуль.ом_Коллекция().СтруктураСвойство(Коллекция, Свойство, Значение);
	
КонецФункции // СтруктураСвойство 

#КонецОбласти

// BSLLS:EmptyRegion-on
// BSLLS:NestedFunctionInParameters-on
 